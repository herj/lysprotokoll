% lysprotokoll.cls
% För protokollskrivning i Lysator, motsvarande.
% 2013-05-13 - Claes Hardesköld (herj) 
% 2014-05-05 - Bilagor nu i klassen (herj)
% 2017-09-11 - Nu med automatisk föredragningslista (herj)
% 2018-11-01 - Nu med referenser till bilagor och frånvaro (herj)
% 2018-11-01 - La till så man kan ha protokollsnummer och diarenummer (herj)
% 2018-11-02 - Omröstningskommando (herj)
% 2018-11-02 - Kommandon för utskrift av närvaro- och frånvaro (herj)

\ProvidesClass{lysprotokoll}[2018/11/02 Lysators protokollklass]
\NeedsTeXFormat{LaTeX2e}

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions
\LoadClass{article}

%{{{ Packet och sånt
\RequirePackage[a4paper]{geometry} 
\RequirePackage{fancyhdr}
%\RequirePackage{lastpage}
\RequirePackage[T1]{fontenc}
\RequirePackage[swedish]{babel}
\RequirePackage[utf8]{inputenc}
\RequirePackage{amssymb}
\RequirePackage{amsmath}
\RequirePackage{tocloft}
%}}}

%{{{ Längder
\setlength{\parskip}{2ex}
\setlength{\parindent}{0em} %0pt}
\setlength{\headheight}{40pt}
\setlength{\headsep}{12pt}
%}}}

%{{{ Räknare
\newcounter{punkter}
\newcounter{underpunkter}[punkter]
\newcounter{underunderpunkter}[underpunkter]
\newcounter{bilagor}
\newcounter{lysbilaga}
\newcounter{yrkande}

%\setcounter{lysbilaga}{1}
%}}}

%{{{ Mötes nummer och sånt
\def\@lysator{Lysator}
\def\@dianr{SPXX-XXXX}
\def\@protokoll{XX}
\def\@datum{2013-02-31}
\def\@plats{\mytilde}
\def\@ordf{Ordf}
\def\@ordfin{Ordf}
\def\@sekr{Sekr}
\def\@sekrin{Sekr}
\def\@just{Just}
\def\@justin{Just}
\def\@justn{Just2}
\def\@justinn{Just2}
\def\nrjust{1}

%\newsavebox{\bilaganr}

\newcommand{\setlysator}[1]{\def\@lysator{#1}}
\newcommand{\setdianr}[2][\@dianr]{\def\@protokoll{#1} \def\@dianr{#2}}
\newcommand{\setdatum}[1]{\def\@datum{#1}}
\newcommand{\setplats}[1]{\def\@plats{#1}}
\newcommand{\setordf}[2]{\def\@ordf{#1} \def\@ordfin{#2}}
\newcommand{\setsekr}[2]{\def\@sekr{#1} \def\@sekrin{#2}}
\newcommand{\setjust}[2]{\def\@just{#1} \def\@justin{#2}}
\newcommand{\setjustn}[2]{\def\@justn{#1} \def\@justinn{#2}}
\newcommand{\setnrjust}[1]{\def\nrjust{#1}}

\newcommand{\lysator}{\@lysator}
\newcommand{\dianr}{\@dianr}
\newcommand{\protokoll}{\@protokoll}
\newcommand{\datum}{\@datum}
\newcommand{\plats}{\@plats}
\newcommand{\ordf}{\@ordf}
\newcommand{\sekr}{\@sekr}
\newcommand{\just}{\@just}
\newcommand{\justn}{\@justn}
%}}}

%{{{ Huvud och fot
\pagestyle{fancy}
\fancyhf{}                            
\renewcommand{\headrulewidth}{0.25pt} %% Thin header line.
\renewcommand{\footrulewidth}{0.25pt} %% Thin header line.

\fancyhead[L]{Protokoll för \lysator\\Protokoll \protokoll\\ \datum , \plats}
\fancyhead[R]{D-nr:\ \texttt{\dianr} \\ \ \\ Sida \thepage \ av
\pageref{UnderskriftsSida}}
\fancyfoot[L]{\ \\ \ \\ \ \\ \line(1,0){50} \\ \hspace{0.5cm}\@ordfin}
\fancyfoot[C]{\if\nrjust1
\ \\ \ \\ \ \\ \line(1,0){60} \\ \@sekrin
\else
\ \\ \ \\ \ \\ \line(1,0){60} \hspace{2cm} \line(1,0){60}
\\ \@sekrin \hspace{3cm} \@justinn
\fi}
\fancyfoot[R]{\ \\ \ \\ \ \\ \line(1,0){50} \\ \@justin\hspace{0.5cm}}
%}}}

%{{{ Övriga bra ha saker
\newcommand{\mytilde}{$\scriptstyle\mathtt{\sim}$}
\newcommand{\hatt}{$\scriptstyle\mathtt{\land}$}
%}}}

%{{{ Närvaro och frånvaro
\def\@medlem{Ingen}
\def\@motlem{Ingen}
\newcommand{\narvarande}[1]{\def\@medlem{#1}}
\newcommand{\visa@narvarande}{\@medlem}
\newcommand{\franvarande}[1]{\def\@motlem{#1}}
\newcommand{\visa@franvarande}{\@motlem}

\newcommand{\narvaro}{\@ifstar
    \narvaro@star
    \narvaro@nostar}
\newcommand{\narvaro@star}{
    \narvaro@nostar\newline
    \franvaro}
\newcommand{\narvaro@nostar}{
    \textbf{Närvarande:}\ \visa@narvarande}

\newcommand{\franvaro}{
    \textbf{Frånvarande:}\ \visa@franvarande}
%}}}

%{{{ Punkter av alla slag
\newcommand{\punkt}[1]{
 	\refstepcounter{punkter}
    \addpkt{\arabic{punkter}}{#1}
	\large{\textbf{\arabic{punkter} #1}}
	\normalsize}

\newcommand{\underpunkt}[1]{
 	\stepcounter{underpunkter}
    \addupkt{\arabic{punkter}.\alph{underpunkter}}{#1}
	\large{\textbf{\arabic{punkter}.\alph{underpunkter} #1}}
	\normalsize}

\newcommand{\underunderpunkt}[1]{
 	\stepcounter{underunderpunkter}
    \adduupkt{\arabic{punkter}.\alph{underpunkter}.\roman{underunderpunkter}}{#1}
	\large{\textbf{\arabic{punkter}.\alph{underpunkter}.\roman{underunderpunkter} #1}}
	\normalsize}

\def\@pktref{}
\newcommand{\setpktref}[1]{\def\@pktref{#1}}
\newcommand{\pktref}{\@pktref}

\newcommand{\punktref}[2][]{
	\stepcounter{bilagor}
 	\refstepcounter{punkter}
    \if\relax#1\relax
        \setpktref{bilaga\arabic{bilagor}}
    \else
        \setpktref{#1}
    \fi
    \addpkt{\arabic{punkter}}{#2}
	\large{\textbf{\arabic{punkter} #2}} (\textit{Bilaga \arabic{bilagor}: \dianr.\arabic{punkter}})
    \label{\pktref}
	\normalsize}

\newcommand{\underpunktref}[1]{
 	\stepcounter{underpunkter}
    \stepcounter{bilagor}
    \if\relax#1\relax
        \setpktref{bilaga\arabic{bilagor}}
    \else
        \setpktref{#1}
    \fi
    \addupkt{\arabic{punkter}.\alph{underpunkter}}{#1}
	\large{\textbf{\arabic{punkter}.\alph{underpunkter} #1}}
    (\textit{Bilaga \arabic{bilagor}:
    \dianr.\arabic{punkter}.\alph{underpunkter}})
	\normalsize}

\newcommand{\underunderpunktref}[1]{
 	\stepcounter{underunderpunkter}
    \stepcounter{bilagor}
    \if\relax#1\relax
        \setpktref{bilaga\arabic{bilagor}}
    \else
        \setpktref{#1}
    \fi
    \adduupkt{\arabic{punkter}.\alph{underpunkter}.\roman{underunderpunkter}}{#1}
	\large{\textbf{\arabic{punkter}.\alph{underpunkter}.\roman{underunderpunkter} #1}}
    (\textit{Bilaga \arabic{bilagor}: \dianr.\arabic{punkter}.\alph{underpunkter}.\roman{underunderpunkter}})
	\normalsize}
%}}}

%{{{ Yrkande, beslut, ordningsfråga
\newcommand{\yrkande}{\@ifstar
    \yrkande@star
    \yrkande@nostar}
\newcommand{\yrkande@star}{
    \underline{Yrkande:}\ }
\newcommand{\yrkande@nostar}{
    \stepcounter{yrkande}
 	\underline{Yrkande \arabic{yrkande}:}\ }

\newcommand{\beslut}{\@ifstar
    \beslut@star
    \beslut@nostar}
\newcommand{\beslut@star}[3][]{
    \setcounter{yrkande}{0}
    \if\relax#1\relax  
        \setavstod{}
    \else
        \setavstod{; Avstår:\ #1}
    \fi
 	\textbf{Beslutades:\ }\textit{(För:\ #2; Emot:\  #3\avstod)}\ }
\newcommand{\beslut@nostar}{
    \setcounter{yrkande}{0}
 	\textbf{Beslutades:}\ }

\def\@avstod{0}
\newcommand{\setavstod}[1]{\def\@avstod{#1}}
\newcommand{\avstod}{\@avstod}

\newcommand{\ordning}{
 	\textit{Ordningsfråga:}\ }
%}}}

%{{{ Underskrifter
\newcommand{\underskrifter}{
    \vspace{2cm}
  	\parbox{210pt}{\hrulefill\\ \ordf, mötesordförande\hfill}

    \vspace{2cm}
    \parbox{210pt}{\hrulefill\\ \sekr, mötessekreterare\hfill}

    \vspace{2cm}
    \if\nrjust2
        \parbox{210pt}{\hrulefill\\ \justn, justeringsperson\hfill}
        \vspace{2cm}
    \fi

    \parbox{210pt}{\hrulefill\\ \just, justeringsperson\hfill}

    \label{UnderskriftsSida} \newpage}
%}}}

%{{{ Bilagor
\newcommand{\bilagaref}[1]{\dianr.\ref{#1}}
\newenvironment{bilaga}[3][\arabic{lysbilaga}]
    {   \setcounter{page}{1}
        \stepcounter{lysbilaga}
        \fancyhead[L]{#2 \\ \ }
        \fancyhead[R]{Bilaga #1 (\arabic{page}/\pageref{bil:\arabic{lysbilaga}}) 
            \\ D-nr: \texttt{#3}}
        \bgroup }
    {   
        \egroup
        \label{bil:\arabic{lysbilaga}}
        \newpage }
%}}}

%{{{ Automatisk föredragningslista
\newlistof{pkter}{forelista}{Föredragningslista}
\newcommand{\addpkt}[2]{
    \stepcounter{pkter}
    \addcontentsline{forelista}{pkter}
        {#1. #2} 
}
\newcommand{\addupkt}[2]{
    \stepcounter{pkter}
    \addcontentsline{forelista}{pkter}
        {\quad #1. #2} 
}
\newcommand{\adduupkt}[2]{
    \stepcounter{pkter}
    \addcontentsline{forelista}{pkter}
        {\qquad #1. #2} 
}
\cftpagenumbersoff{pkter}
\newcommand{\foredragningslista}{
    \listofpkter
    \thispagestyle{fancy}
}
%}}}
